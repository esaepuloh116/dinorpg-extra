Méthode "économique" pour upper vos dinoz :

Voici une FAQ pour vous aider à upper vos dinoz de façon à ne pas perdre trop de déplacements inutiles ni de temps, en particulier en tant que non codeur, pour upper plus ou moins rapidement vos dinoz...
Il y a sans doute beaucoup d'autres méthodes, inversant l'ordre dans lequel je vous propose de réaliser les missions, néanmoins pour le réaliser en ce moment même, je peux vous dire que pour le moment c'est plutôt intéressant ^^

1ère partie :

- Au level 1, tous mes dinoz réalisent leur combat quotidien seuls à la fontaine de jouvence (de façon à gagner les 5 pv avec la perle tous les jours). Ainsi ils gagnent 25 d'xp au lieu de 20 par groupe de 3, et uppent en 4 jours au lieu de 5...

- A partir du level 2, je les groupe par 3, et je continue de les faire combattre tous les jours à la fontaine de jouvence, jusqu'au level 7.

- Dès lors, je commence les missions de papy joe, et quand je les ai finies, je vais battre les 2 gardiens à l'université!

- Normalement, à ce moment-là, mes dinoz sont aux alentours du niveau 11 (si ce n'est pas le cas je vais à la fontaine leur faire regagner des PV jusqu'à ce qu'ils soient niveau 11) et dès le niveau 11 atteint, je commence la mission des Géants de Papy joe.

- Tout en réalisant cette mission, je vais récupérer la pelle aux mines de corail, puis je vais aux chutes mutantes (parler au garde atlante), en creusant au marais en chemin, afin de récupérer le 1er ingrédient nécessaire à la fabrication du gant des zors. Je retourne donc au mine de corail ensuite réparer la pelle, puis direction les pentes de basaltes! En chemin je m'arrête au port de prêche battre le rascaphandre afin de récupérer l'appeau qui me servira plus tard (et empocher les 75 d'xp environ qu'il donne à ce level)... Une fois récupéré l'ingrédient aux pentes de basalte, je retourne réparer une nouvelle fois la pelle, puis je vais à la fontaine récupérer le dernier ingrédient!

- Une fois tous les ingrédients réunis, je vais une dernière fois réparer la pelle aux mines, puis je file au Dôme Soulaflotte battre le gardien et récupérer le Gant des Zors!

- Après quoi, je passe au chutes parler au moine Hydargol afin de récupérer la feuille de nénuphar, qui me permet dans la foulée de partir à la forêt de Grumhel récupérer la Lanterne auprès le l'Homme bizarre! (normalement à ce niveau là, 13-14, on ne rencontre que des korgons ou des roncivores, donc pas de soucis)

- Je retourne ensuite à la fontaine de jouvence, où je finis la mission des géants avec mes 3 dinoz, tout en récupérant des PV qui me seront utiles pour la suite, avant de la valider!

- Une fois niveau 15-16, je retourne à la forêt de Grumhel, mais par le grand tout chaud cette fois, parler à Diane Korgsey pour faire la première mission qui me rapportera les palmes (= aller-retour du Camp Korgon aux Ruines d'Ashpouck).

- Une fois les palmes en poche, je vais voir le shaman à Fosselave et je fais toutes ses mission, SAUF "Commerce peu équitable" et "Querelles" ! (passage au niveau 20-21)

- Et j'enchaine dans la foulée toutes les missions du maître pour récupérer les sphères et passer aux alentours du niveau 24 au final!


-------


Fin 1ère partie : A partir de là plusieurs choix s'offrent à vous...

- Pour un dinoz à congeler, pour les compétences uniques, je dirais que c'est maintenant ou jamais!

- Pour un dinoz à sacrifier à plus haut level ou à upper pour participer aux PVP, explorations etc... On continue avec la 2ème partie...


-------


2ème partie :

- Pour commencer, je retourne voir papy joe et je fais la mission de l'exploit du mois! Afin de gagner 200 d'xp et 8000 d'or toujours bon à recevoir (en faisant bien entendu combattre mes dinozs à la fontaine pour gagner des PV)!

- Maintenant, après ça, on peut aller parler au Rodeur Etrange aux Forges du grand tout chaud pour faire sa première mission, (vous êtes aux alentours du niveau 25-26), et on part se concentrer chez Bao-Bob pour entrer dans le monde sombre!!!

- Vous (oui, je passe au vouvoiement) restez là-bas tant que vous gagner une quantité d'xp suffisante en combattant des Amenpennes (jusqu'à 45-55 d'xp pour un groupe de 3 à ce niveau c'est très très correct!) puis vous aller battre le gardien du monde Sombre pour vous retrouver au Marais!

- Une fois là vous battez tous les piranhoz pour aller retourner ensuite valider la mission du rodeur aux Forges! Là vous pouvez faire la seconde mission tout de suite, soit la garder pour plus tard, comme elle rapporte 250 d'xp et 5000 d'or, c'est toujours bon à prendre!

- Vous voilà je pense pas très loin du niveau 29-30, c'est le moment idéal pour aller aux Steppes magnétiques! Néanmoins avant cela je vous conseillerai au moins de faire la première mission du Gardien de la forêt au fond de la forêt de Grumhel, car elle rapporte pas mal d'xp et que c'est la plus longue de cette liste de mission, elle vous fera donc sans doute gagner un autre niveau!

- Ensuite donc, direction les steppes si vous n'avez pas un groupe composé essentiellement de dinoz (sinon vous les séparerez et les ferez rentrer avec d'autres groupe individuellement plus tard! En attendant, envoyez-les par exemple commencer le tournoi de Forcebrut!)

- Aux steppes vous enchainer toutes les missions du bureau de requête! Je vous conseille de terminer par la mission "Sécurisez la croisée" car elle est longue et une fois que vos dinoz auront gagner 2-3 levels ce sera une banalité! La mission du vermifuge, si vous avez un dinoz dans votre groupe est à faire à la fin également, car elle peut couter très cher en soins si vos autres dinoz ne compense pas le malus dû au dinoz (absorption des lombrik)...
Mais profiter à fond des mission de "mercenaires" pour battre les voleurs du coin! En enchainant les combats aux memes endroits avec une action sur chaque dinoz du groupe en changeant de leader, il y a beaucoup d'xp et d'argent à se faire!

- Vous sortirez de là environ level 33-34, en attendant la suite!


-------


Fin de la 2ème partie : A partir de là plusieurs choix s'offrent à vous...

- Xp rapidement et aller directement à Nimbao faire la fête!

- Finir des quêtes qui peuvent s'avérer utiles avant d'y aller...


-------

3ème partie :

- Si vous décidez d'attendre un peu avant de rejoindre Nimbao, je pense que la première chose à faire est d'aller faire les mission de Nicolas Mulot! Le sac à dos très pratique permet de stocker un objet supplémentaire et en PVP (comme vos dinoz commencent quand même a se défendre vu leur level) c'est très intéressant! Je les ferais donc en priorité!

- Après quoi vous pouvez terminer les missions du Gardien de la Forêt (notamment si vous n'avez pas fait la quête de l'éclat de magnétite négative)!

- Maintenant, vous ne serez plus très loin du level 35, vous pouvez enfin vous rendre sur Nimbao!!! Où l'xp vous attend pour monter vos dinoz le plus haut level possible, avec les mission de Chen et la quête principale qui sont généreuses en xp...

- Aux alentours du niveau 40, il pourrait s'avérer utile d'aller faire toute la quête de l'île au monstre afin d'y récupérer le gant de capture, il vous aidera beaucoup à xp sans perdre de pv à nimbao pour les derniers levels! Vous pourrez aussi vous faire tout le tournoi de Forcebrut ainsi que le vénérable.

- Après si vous voulez vous amusez et faire d'autres quêtes à l'occasion, have fun !


--------


Je m'arrête là, je pense avoir fait le tour! Après je ne parle pas des mission de Bao Bob, si vous voulez les faire libre à vous de les faire quand vous voulez, elles ne sont pas compliquées et n'apporte rien de particulier si ce n'est sans doute 1 niveau et le maillot du tour de Dinoland (pour les chasseurs de trophés!)

Sur ce j'attends des réactions, il y a sans doute des améliorations à apporter!

Uvo.