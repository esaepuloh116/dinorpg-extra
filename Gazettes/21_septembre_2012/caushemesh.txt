Depuis le temps qu'on entendait parler de Caushemesh, du niveau 60, de nouveaux dinoz... La MT l'a fait ! Outre la nouveauté annoncée, c'est tout le jeu qui va évoluer pendant les mois à venir.


En fait, l'évolution a déjà commencé en corrigeant de nombreux bugs déjà signalés avant la mise à jour de la nouveauté. Quitte à bloquer le site plusieurs heures, autant en profiter pour tout faire n'est-ce pas ? Certains bugs sont apparus, liés aux nouveauté, mais l'équipe de développement s'est efforcé à corriger au plus vite les bugs signalés par les joueurs, pour un plaisir de jeu intact.


Autre point à signaler avant d'attaquer le gros morceau de la nouveauté, le retour tant attendu des packs d'abonnement.
Pour ceux qui n'en auraient pas encore entendu parler, ces pack d'abonnement (payants) proposent sur des périodes de un à 6 mois des avantages exclusifs à ces packs :

- la possibilité de stocker jusqu'à 20% en plus d'objets et d'ingrédients en tout genre. Pratique notamment sur certaines missions gourmandes en ingrédients rares (on me souffle dans l'oreille les énergies  pour une des missions de Gullom)
- la conversion de votre or en bons du trésor, à raison de 100 bons obtenus ainsi par mois, pour le coût - certes onéreux - de 1250 pièces par bon.
- la possibilité de changer par chirurgie esthétique l'apparence de votre dinoz, à raison d'une opération par mois maximum. Attention ! La nouvelle apparence obtenue est totalement aléatoire !
- L'élargissement du choix des dinozs à l'achat dans les enclos des dinozs normaux et démoniaques.

La raison première d'acheter ce pack reste néanmoins l'obtention de 3 potions spéciales d'irma, qui s'utilisent comme les potions d'irma classiques, à ceci près qu'elles ne sont pas cumulables ! Ainsi, si vous oubliez de les utiliser pendant une journée... elles sont irrémédiablement perdues. C'est le premier bémol de ces pack élite. On espère avoir la possibilité, dans un avenir proche, de pouvoir les cumuler, pour un plaisir de jeu encore grandi.

Dernier avantage, qui concerne uniquement le pack 6 mois à 30 euros, l'obtention d'un oeuf de dinoz inédit : le Soufflet ! Ce dinoz ne peut s'obtenir pour le moment que par ce biais, ou au Marché des éleveurs.
Ce dinoz, à la croisée physiquement d'une chenille de papillon et d'un Snorky, peut porter jusqu'à 2 objets magiques différents. Et comme le Quetzu, un élément a disparu dans sa grille des éléments, ici le feu. Les soufflets n'apprendront donc aucune compétence dans cet élément ! A voir si cette particularité en fera un dinoz prisé à haut niveau, malgré sa rareté.

Le 2è bémol de ces packs élite, mais non des moindre, c'est le mode de paiement. Si tous sont accessibles via une carte bancaire ou un compte paypal, tous ne le sont pas par paiement téléphonique / internet...
Ainsi, n'escomptez pas obtenir pour l'heure un soufflet avec votre crédit de téléphone, ce n'est pour l'instant pas possible ! Cette décision n'appartient néanmoins pas uniquement à la MT, puisque les opérateurs proposant ces paiements se rémunèrent au passage, ce qui entraîne des surcoûts bien plus élevés que pour un utilisateur de carte bancaire par exemple.

Malgré tout, ces packs restent de bonnes affaires : les 3 potions d'irma par jour valent à elles seules le détour, et rentabilisent largement l'achat de ces packs, pour peu que vous soyez un joueur assidu.


En parlant d'assiduité, justement, un nouveau système est apparu : le compteur temps. Si les dinoz de niveau 20 ou plus ont toujours droit à une action gratuite par jour, les dinoz de niveau 1 à 19 se voient offrir des actions gratuites toutes les X heures, X étant leur niveau actuel.
Ainsi, un dinoz de niveau 1 peut être joué toutes les heures, un dinoz de niveau 5 toutes les 5 heures, et ainsi de suite.
Les nouvelles actions des dinozs sont visibles avec un sablier à droite de leur nom. Un raccourci d'action appelé à juste titre "nouvelles actions" placé juste au-dessus de celui de la "boutique" permet d'activer tous les sabliers visibles en même temps, ce qui donne à ces dinoz la possibilité de rejouer, selon votre guise.


Concernant le jeu en lui-même, qu'apporte cette mise à jour me direz-vous ? La possibilité, ni plus ni moins, de débloquer le niveau 60 pour vos dinoz, sur fond de quête épique sur le continent de Caushemesh (avec ses décors pittoresques, et ses autochtones à affronter), avec à la clé la découverte de 78 nouvelles compétences !
Mais ce n'est pas tout : l'arrivée en fanfare d'un nouveau paramètre de combat : l'endurance.

L'endurance, représentée par une barre d'énergie bleue, représente la fatigue de votre dinoz. Et entre pleinement dans le rééquilibrage vitesse / initiative tant demandé par les joueurs de haut niveau.
En effet, un dinoz avec une grande initiative va attaquer et dépenser rapidement son endurance, en enchaînant plusieurs assauts et compétences (A, E, invocations). Mais si la barre se régénère petit à petit, entre chaque tour de combat, si cette barre est vide, le dinoz est essouflé et en passe son tour.

Rien ne sert de courir, il faut partir à point ? Tel serait la devise du système de combat avec cette nouvelle donnée. Et si certaines compétences influeront sur cette jauge d'endurance, on peut imaginer que les nouvelles compétences vont certainement chambouler les stratégies de combat, et les plans de up déjà largement éprouvés.
De là à faire revenir sur le devant de la scène certaines races de dinoz délaissés, c'est encore trop tôt pour le dire. Néanmoins, l'attente est grande, et leur découverte en est encore à ses balbutiements.

On rappellera par ailleurs que si vous pouvez continuer à choisir au delà du niveau 50 des compétences de l'ancien arbre de compétences, si vous changer d'arbre pour apprendre les nouvelles compétences, vous ne pourrez plus faire marche arrière.
Par exemple, vous pourrez tenter d'apprendre sumo à votre wanwan adoré au niveau 51 (ou jusqu'au niveau 60, si vous êtes têtu ^^), mais si vous lui apprenez des compétences du nouvel arbre de compétences, vous ne pourrez pas revenir apprendre plus tard sumo...


Enfin, pour les amoureux des sciences occultes, sachez que vous pourrez débloquer de nouveaux dinoz démons, dont les moueffes et kabukis comme annoncés sur l'image de présentation de la nouveauté. elle est pas belle la vie ?