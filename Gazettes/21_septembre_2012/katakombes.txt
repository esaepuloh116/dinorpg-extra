L’existence des Katatombes, dévoilée au grand public le 24 mars 2009 a bouleversé le destin de plus d’un maitre Dinoz…

En effet, cet événement à marqué l’existence des éleveurs par l’apparition de nouveaux dinoz dits « Démons ». Bon nombre des Sages de Dinoland se sont longtemps questionnés sur l’origine de ces monstres.
Ces derniers mois, alors que les batailles entre les clans faisaient rage, nous avons envoyé une équipe entraînée pour survivre plusieurs semaines dans le labyrinthe avec pour but de lever enfin le voile, en allant directement étudier aux sources du mystère.
Le chef des opérations, et doyen, le dénommé « Pikun-Criz » mena l’équipe d’une poigne de fer jusqu’au dernier sous-sol, et découvrit une fresque, dans le bas-relief d’un mur, gravée d’étranges signes.
« Ōȵɕə Ħɞƪȝ »
Un des chercheurs eut une étrange convulsion, puis un éclair de lumière jaillit par les pierres de sol, et celui-ci s’effondra.
Tous tombèrent sur une chose qui libérait une force divine. Une écorce noire, des feuilles rouges sang, il s’agissait du légendaire Baobabaw… Pikun-Criz fit observer à ses confrères la présence de cocons qui était accumulés entre les bras du géant qui gisait sous leurs corps.
Un flash aveugla les chercheurs, puis, alors que deux d’entre eux souffraient et partaient vers l’autre monde, les survivants se retrouvèrent à la surface, sans pouvoir redescendre les marches du Labyrinthe.

Ces informations ont permises à Pikun-Criz de fournir au roi un rapport admettant l’hypothèse que les Démons naissaient de ces cocons, mis au monde par un corps mystique.
Malheureusement, aucune autre information n’a pu être récoltée, mais celles-ci nous permettent tout de même de tenir un discours cohérent sur l’existence de ces bêtes étranges.


Ce texte gardé aux archives de la gazette depuis quelques temps déjà, pourrait à présent intéresser de nombreux maîtres dinoz depuis l'arrivée de Caushemesh dans le paysage dinolandien. En effet, il existerait d'autres dinoz démons... Susceptibles d'être étudiés ou dressés prochainement.